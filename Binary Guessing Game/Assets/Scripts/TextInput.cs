﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextInput : MonoBehaviour {

	public Text output; 
	public Text computerOutput;
	public Text timerText;


	InputField input; 
	InputField answer;
	InputField.SubmitEvent subEvent;
	InputField.SubmitEvent answerEvent;


	// variables used to determine the user's number
	int start = 0; 
	int end = 1001; 
	int guess;
	int one = 0; 
	int two = 1001; 

	// used to advance the dialogue
	int trigger = 0;

	// variables that control delays
	float timeLeft;
	float dur; 

	// booleans that help to advance the play
	bool greaterThan;
	bool switching = true; 
	bool yesOrNo;
	bool didNotUnderstand = false;
	bool started = false;

	// strings for the computer's conversation
	string response;
	string conversation; 
	string attempt = "";
	string newText;





	/*
	 * ------------------------------- Start() -------------------------------------
	 * */


	void Start () 
	{
		conversation = "Hello. I am the world's greatest number guesser. \nI ALWAYS WIN.\n";
		computerOutput.text = conversation;

		input = gameObject.GetComponent<InputField> ();
		subEvent = new InputField.SubmitEvent ();
		subEvent.AddListener (SubmitInput); // this is method that waits for an event
		input.onEndEdit = subEvent; // This sets up which event to call when the listener "hears" when the user hits enter
		subEvent.AddListener (StartAsking); // this is method that waits for an event

		trigger = 1;

	}

	/*
	 * ------------------------------- Update() -------------------------------------
	 * */

	void Update ()
	{
		attempt = newText;
		if (trigger ==1) {
			response = "Shall we play? Yes or no?.";
			Debug.Log ("Yes or no before Asks() = " + yesOrNo);
			StartCoroutine(Responding ());
			StartAsking (response);
			trigger++;
		} // trigger == 1

		Debug.Log ("We have returned to Update()");
		
	} // end Update


	/*
	 * ------------------------------- Custom Functions -------------------------------------
	 * */

	private void SubmitInput(string arg0) //This takes the inputted text and places it in the output window
	{
		string currentText = output.text; // .ToString (); // this will convert the Text type to a String
		newText = currentText + "\n" + arg0 + "\n\n";
		output.text = newText; 

		input.text = ""; // clears out the text
		input.ActivateInputField(); // This activates the input field again. 
	} // end SubmitInput
		
		
	IEnumerator Responding() // This prints out our script
	{ 
		yield return new WaitForSeconds(2.0f);
		conversation += "\n";
		conversation += response;
		conversation += "\n";
		computerOutput.text = conversation;
		yield break;
	} // end Responding

	IEnumerator MoveText() // This is a custom function to essentially scroll the text up
	{ 
		yield return new WaitForSeconds(0.0f);

		GameObject moveTextC = computerOutput.gameObject;
		GameObject moveTextO = output.gameObject;

		// here we move up our overly long text windows to give the impression of scrolling text
		moveTextC.transform.Translate(0, 0.5f, Time.deltaTime);
		moveTextO.transform.Translate(0, 0.5f, Time.deltaTime);
		moveTextC.transform.Translate(0, Time.deltaTime, 0, Space.World);
		moveTextO.transform.Translate(0, Time.deltaTime, 0, Space.World);
	} // end MoveText

	private void StartAsking(string arg1) 
	{
		StartCoroutine(Asks());
	}

	IEnumerator Asks() // This function determines if the user said yes, no, or anything else
	{ 
		yield return new WaitForSeconds(0.0f);

		Debug.Log ("I am input.text + " + attempt);

		Debug.Log (attempt);

		if (attempt.Trim ().Equals ("")) { // this happens at the very beginning
			Debug.Log ("In attempt == ''");
			didNotUnderstand = false;


		} else if (attempt.Trim ().Equals ("Y") || attempt.Trim ().Equals ("y") || attempt.Trim ().Equals ("Yes") || attempt.Trim ().Equals ("yes")) {
			Debug.Log ("In attempt == yes");
			yesOrNo = true;
			didNotUnderstand = false;
			attempt = "";
			StartCoroutine (MoveText ());
			response = "Excellent. Let's begin. \nIs your number greater than 500?";
			StartCoroutine (Responding ());
			yield break;

		} else if (attempt.Trim ().Equals ("N") || attempt.Trim ().Equals ("n") || attempt.Trim ().Equals ("No") || attempt.Trim ().Equals ("no")) {
			Debug.Log ("In attempt == no");
			yesOrNo = false;
			didNotUnderstand = false;
			StartCoroutine (Responding ());
			response = "Okay, so you selected NO";
			yield break;

		} else {
			response = "I'm sorry. That made literally no sense. \nI'll ask again: Yes or no? ";
			didNotUnderstand = true;
			StartCoroutine (MoveText ());
			StartCoroutine (Responding ());
			yield break;


		} // end if/else 


	} // end Asks 


} // end class

